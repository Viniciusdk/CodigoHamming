import java.util.Scanner;

public class CodigoHamming {
	
public static void main(String[] args) {
		
	{
		Scanner ip = new Scanner(System.in);
		System.out.print("Digite os Bites: ");
		String msg = ip.next();
		int r=0;
		int m=msg.length(); 
		int contador = 0;
		
		while(true)
		{
			if(m+r+1<=Math.pow(2,r))
			{
				break;
			}
			r++;
		}
		System.out.println("Numero de paridade que os bits precisam: "+r);
		
		int transLength = msg.length()+r,temp=0,temp2=0,j=0;
		int transMsg[]=new int[transLength+1];
		for(int i=1;i<=transLength;i++)
		{
			temp2=(int)Math.pow(2,temp);
			if(i%temp2!=0)
			{
				transMsg[i]=Integer.parseInt(Character.toString(msg.charAt(j)));
				j++;
			}
			else
			{
				temp++;
			}
		}
		for(int i=1;i<=transLength;i++)
		{
			System.out.print(transMsg[i]);
		}
		System.out.println("\n");	

		for(int i=0;i<r;i++)
		{
			int smallStep=(int)Math.pow(2,i);
			int bigStep=smallStep*2;
			int start=smallStep,checkPos=start;
			System.out.println("Calculando a paridade dos bites pela posicao : "+smallStep);
			System.out.print("Bites a serem checados : ");
			while(true)
			{
				for(int k=start;k<=start+smallStep-1;k++)
				{
					checkPos=k;
					System.out.print(checkPos +  " ");
					
					if(k>transLength)
					{
						break;
					}
					transMsg[smallStep]^=transMsg[checkPos];
						if(transMsg[checkPos] == 1) {
							contador++;
						}
				}
					if(checkPos>transLength)
					{
						break;
					}
					else
					{
						start=start+bigStep;
					}
			}
			if(contador%2 == 0) {
				System.out.println("\n \nA Posicao é Par!");
			}
			else {
				System.out.println("\n \nA Posicao é Impar!");
			}
		
			System.out.println("\n");
			contador = 0;
		}
		System.out.print("Mensagem Codificada de Hamming: ");
		for(int i=1;i<=transLength;i++)
		{
			System.out.print(transMsg[i]);
		}
		System.out.println();
	}
}
}